package demo.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.stereotype.Component;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;

@Component
public class GalaxyApiConfig {

    final Logger logger = LoggerFactory.getLogger(GalaxyApiConfig.class);

    @Value("${api.baseUri}")
    String baseUri;

    @Value("${api.port}")
    Integer port;

    public RequestSpecification init(RestDocumentationContextProvider restDocumentation) {
        logger.info("Setting up request spec baseUri={};    port={};", baseUri, port);

        RequestSpecification documentationSpec = new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setPort(port)
                .addFilter(documentationConfiguration(restDocumentation))
                .addFilter(document("{method-name}"))
                .build();
        return given(documentationSpec);
    }

}
