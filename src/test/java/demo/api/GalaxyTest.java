package demo.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;

import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith({RestDocumentationExtension.class})
public class GalaxyTest {

    final Logger logger = LoggerFactory.getLogger(GalaxyTest.class);

    private RequestSpecification api;

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentation,
            @Autowired GalaxyApiConfig requestConfig) {
        this.api = requestConfig.init(restDocumentation);
    }

    @Test
    public void stars() {
        logger.info("Getting stars...");
        api.get("/stars")
                .then()
                .assertThat()
                .statusCode(is(200));
    }

    @Test
    public void constellations() {
        logger.info("Getting constellations...");
        api.get("/constellations")
                .then()
                .assertThat()
                .statusCode(is(200));
    }

}
