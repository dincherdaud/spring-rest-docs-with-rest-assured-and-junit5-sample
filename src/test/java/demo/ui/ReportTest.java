package demo.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import io.github.bonigarcia.seljup.SeleniumExtension;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@ExtendWith(SeleniumExtension.class)
public class ReportTest {

    final Logger logger = LoggerFactory.getLogger(ReportTest.class);

    @Value("${ui.baseUri}")
    String baseUri;

    @Test
    public void successRate100(ChromeDriver driver) {
        String uri = baseUri + "/reports/index.html";
        logger.info("Navigate to {}", uri);
        driver.get(uri);
        WebElement successRatio = driver.findElement(By.id("successRate")).findElement(By.tagName("div"));
        String actual = successRatio.getText();
        logger.info("Actual result {}", actual);
        String expected = "100%";
        assertEquals(expected, actual);
    }

}
